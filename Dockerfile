FROM node:14-alpine

WORKDIR /var/www/node

COPY . .

RUN npm install

RUN npm run build

CMD npm run start:prod