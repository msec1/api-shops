require('dotenv').config();

import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './modules/app.module';
import * as basicAuth from 'express-basic-auth';
import * as config from 'config';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import * as helmet from 'helmet';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  // Setup helmet security
  app.use(helmet());
  
  // Setup Validation
  app.useGlobalPipes(new ValidationPipe());

  // Setup Versioning
  app.enableVersioning({
    type: VersioningType.URI,
  });

  // Setup Swagger
  app.use(['/api'], basicAuth({
    challenge: true,
    users: {
      [config.get('swagger.docUsername')]: config.get('swagger.docPassword'),
    },
  }));
  
  const configSwagger = new DocumentBuilder()
  .setTitle('Swagger')
  .setDescription('The api-shops API description')
  .setVersion('1.0')
  .addBearerAuth()
  .build();
  const document = SwaggerModule.createDocument(app, configSwagger);
  SwaggerModule.setup('api', app, document);

  // Setup microservice
  // microservice #1 use Kafka
  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.KAFKA,
    options: {
      client: {
        brokers: [config.get('kafka.uri')],
      }
    }
  });

  await app.startAllMicroservices();

  // App listen
  await app.listen(config.get('server.port'));
}
bootstrap();
