import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ShopModule } from './shop/shop.module';
import { HealthController } from './health/health.controller';
import * as config from 'config';
import { TerminusModule } from '@nestjs/terminus';

@Module({
  imports: [
    TerminusModule,
    MongooseModule.forRoot(config.get('mongodb.uri'), {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
    }),
    ShopModule
  ],
  controllers: [HealthController],
})
export class AppModule {}
