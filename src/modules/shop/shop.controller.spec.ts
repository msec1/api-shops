import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { ShopController } from './shop.controller';

describe.skip('ShopController', () => {
  let controller: ShopController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    controller = module.get<ShopController>(ShopController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
