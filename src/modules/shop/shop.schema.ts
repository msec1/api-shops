import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ShopDocument = Shop & Document;

@Schema()
export class Shop {
  @Prop({
    index: 1,
    require: true,
  })
  type: string;

  @Prop({
    unique: true,
    require: true,
  })
  name: string;

  @Prop({
    index: 1,
    require: true,
  })
  manager: string;

  @Prop({
    default: ''
  })
  logo: string;

  @Prop({
    default: 'active'
  })
  status: string;
}

export const ShopSchema = SchemaFactory.createForClass(Shop);