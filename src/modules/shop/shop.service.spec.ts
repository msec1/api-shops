import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../app.module';
import { ShopService } from './shop.service';

describe.skip('ShopService', () => {
  let service: ShopService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    service = module.get<ShopService>(ShopService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
