import { Controller, Get, Post, Body, Put, Param, Delete, ParseIntPipe, Version } from '@nestjs/common';
import { ShopService } from './shop.service';
import { CreateShopDto } from './dto/create-shop.dto';
import { UpdateShopDto } from './dto/update-shop.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@Controller({
  version: '1',
  path: 'shop',
})
@ApiTags('Shops')
@ApiBearerAuth()
export class ShopController {
  constructor(private readonly shopService: ShopService) {}

  @Post()
  create(@Body() createShopDto: CreateShopDto) {
    return this.shopService.create(createShopDto);
  }

  @Get()
  findAll() {
    return this.shopService.findAll();
  }

  @Get(':id')
  findOne(
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.shopService.findOne(id);
  }

  @Get(':id')
  @Version('2')
  findOneV2(
    @Param('id', ParseIntPipe) id: number,
  ) {
    return this.shopService.findOne(id);
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateShopDto: UpdateShopDto) {
    return this.shopService.update(+id, updateShopDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.shopService.remove(+id);
  }
}
