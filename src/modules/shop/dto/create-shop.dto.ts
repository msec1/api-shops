import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty } from "class-validator";

export class CreateShopDto {
  @ApiProperty()
  type: string;

  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @ApiProperty()
  manager: string;

  @ApiProperty({
    required: false
  })
  logo?: string;

  @ApiProperty({
    required: false
  })
  status?: string;
}
